require('dotenv').config()
const Commando = require('discord.js-commando')
const Discord = require('discord.js')
const path = require('path')

global.servers = {}

const client = new Commando.Client({
    owner: '325288958378377216',
    disableEveryone: true,
    commandPrefix: process.env.PREFIX
})

client.registry
    .registerGroups([
        ['general', 'General commands'],
        ['music', 'Music commands']
    ])
    .registerDefaults()
    .registerCommandsIn(path.join(__dirname, 'commands'))

client.on('ready', () => {
    console.log(`${client.user.username} is online!`)
    client.user.setActivity(`${process.env.PREFIX}help`)
})

client.login(process.env.TOKEN)
