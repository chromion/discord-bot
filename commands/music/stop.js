const commando = require('discord.js-commando')

module.exports = class Stop extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'stop',
            group: 'music',
            memberName: 'stop',
            description: 'Stops the music ;)',
            examples: ['stop']
        })
    }

    async run(msg, args) {

        if (msg.member.voiceChannel) {
            if(!servers[msg.guild.id]) {
                return msg.reply(`There is no music to stop.`)
            }
            servers[msg.guild.id].dispatcher.end()
        } else {
            msg.reply('You must be in a voice channel to summon me!')
        }
    }
}
