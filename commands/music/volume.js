const commando = require('discord.js-commando')

module.exports = class Volume extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'volume',
            group: 'music',
            memberName: 'volume',
            description: 'Turn it up dj!',
            examples: ['volume']
        })
    }

    async run(msg, args) {

        if (msg.member.voiceChannel) {
            if(!servers[msg.guild.id]) {
                return msg.reply(`There is no music to stop.`)
            }
            if(args && args > 0 && args < 100) {
                servers[msg.guild.id].dispatcher.setVolume(args)
                servers[msg.guild.id].botVolume = args
            } else {
                return msg.reply('Are you crazy my ears!')
            }
        } else {
            msg.reply('You must be in a voice channel to summon me!')
        }
    }
}
