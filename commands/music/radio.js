const commando = require('discord.js-commando')
const ytdl = require('ytdl-core')

module.exports = class Radio extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'radio',
            group: 'music',
            memberName: 'radio',
            description: 'Hardstyle never dies!',
            examples: ['radio']
        })
    }

    play(connection, msg) {
        const server = servers[msg.guild.id]
        server.dispatcher = connection.playStream('https://19983.live.streamtheworld.com/Q_DANCE.mp3', {
            volume: server.botVolume,
            passes: 3
        })
        server.dispatcher.on('end', () => {
            connection.disconnect()
        })
    }

    async run(msg, args) {

        if (msg.member.voiceChannel) {
            if (!servers[msg.guild.id]) {
                servers[msg.guild.id] = {queue: [], botVolume: 0.05}
            }
            msg.member.voiceChannel.join()
                .then(connection => {
                    this.play(connection, msg)
                }).catch(console.error)
        } else {
            msg.reply('You must be in a voice channel to summon me!')
        }
    }
}
