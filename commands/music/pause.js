const commando = require('discord.js-commando')

module.exports = class Pause extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'pause',
            group: 'music',
            memberName: 'pause',
            description: 'Silence!',
            examples: ['pause']
        })
    }

    async run(msg, args) {

        if (msg.member.voiceChannel) {
            if(!servers[msg.guild.id]) {
                return msg.reply(`There is no music to pause.`)
            }
            servers[msg.guild.id].dispatcher.pause()
        } else {
            msg.reply('You must be in a voice channel to summon me!')
        }
    }
}