const commando = require('discord.js-commando')

module.exports = class Resume extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'resume',
            group: 'music',
            memberName: 'resume',
            description: 'Keep on listening!',
            examples: ['resume']
        })
    }

    async run(msg, args) {

        if (msg.member.voiceChannel) {
            if(!servers[msg.guild.id]) {
                return msg.reply(`There is no music to resume.`)
            }
            servers[msg.guild.id].dispatcher.resume()
        } else {
            msg.reply('You must be in a voice channel to summon me!')
        }
    }
}