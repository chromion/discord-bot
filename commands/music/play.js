const commando = require('discord.js-commando')
const ytdl = require('ytdl-core')

module.exports = class Play extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'play',
            group: 'music',
            memberName: 'play',
            description: 'Plays music, what a suprise.',
            examples: ['play descpacito']
        })
    }

    play(connection, msg) {
        const server = servers[msg.guild.id]
        server.dispatcher = connection.playStream(ytdl(server.queue[0], {filter: 'audioonly'}), {volume: server.botVolume, passes: 3})
        server.dispatcher.on('end', () => {
            server.queue.shift()
            if(server.queue[0]) {
                this.play(connection, msg)
            } else {
                connection.disconnect()
            }
        })
    }

    async run(msg, args) {

        if (msg.member.voiceChannel) {
            if(!servers[msg.guild.id]) {
                servers[msg.guild.id] = {queue: [], botVolume: 0.05}
            }
            msg.member.voiceChannel.join()
                .then(connection => {
                    servers[msg.guild.id].queue.push(args)
                    if(servers[msg.guild.id].queue.length > 1) {
                        // do nothing else than waiting for the queue
                    } else {
                        this.play(connection, msg)
                    }
                }).catch(console.error)
        } else {
            msg.reply('You must be in a voice channel to summon me!')
        }
    }
}
