const commando = require('discord.js-commando')

module.exports = class Leave extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'leave',
            group: 'general',
            memberName: 'leave',
            description: 'I leave you alone.',
            examples: ['leave']
        })
    }

    async run(msg, args) {
        if (msg.member.voiceChannel) {
            msg.guild.voiceConnection.disconnect()
            delete servers[msg.guild.id]
        } else {
            msg.reply('What the hell im not even here!')
        }
    }
}
