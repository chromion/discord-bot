const commando = require('discord.js-commando')

module.exports = class Restart extends commando.Command {
    constructor(client) {
        super(client, {
            name: 'restart',
            group: 'general',
            memberName: 'restart',
            description: 'Did you tried already to turn your pc off and on again?',
            examples: ['restart']
        })
    }

    async run(msg, args) {
        this.client.destroy().then(() => {
            servers = {}
            this.client.login(process.env.TOKEN)
        })
    }
}
